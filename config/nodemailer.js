module.exports.nodemailer = {
  usessl: false,
  from: process.env.MAIL_USER,
  prepend_subject: false,
  host: process.env.MAIL_PORT,
  user: process.env.MAIL_USER,
  pass: process.env.MAIL_PASS,
  contact: 'jeanr.robles@gmail.com'
}
