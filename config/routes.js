/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  '/': {
    view: 'homepage'
  },

  'get /isauth': {
    controller: 'UserController',
    action: 'isAuthenticated'
  },

  'get /admin': {
    controller: 'AdminController',
    action: 'enter'
  },

  'get /admin/dashboard': {
    view: 'admin/dashboard',
    locals: {
      layout: 'layout_admin'
    }
  },

  'get /user/:id/activate/:token': {
    controller: 'AuthController',
    action: 'activate'
  },

  'get /user/:id/reset/:token': {
    controller: 'AuthController',
    action: 'resetPassword'
  },

  'get /user/:id/newpassword/:token': {
    controller: 'AuthController',
    action: 'newPassword'
  },

  'get /enter': {
    controller: 'AdminController',
    action: 'enter'
  },

  'get /courses/byfilters': {
    controller: 'CourseController',
    action: 'findByFilters'
  },

  'post /courses/upload': {
    controller: 'CourseController',
    action: 'uploadCourse'
  },

  'post /courses/edit': {
    controller: 'CourseController',
    action: 'updateCourse'
  },

  'get /courses/all': {
    controller: 'CourseController',
    action: 'getAllCourses'
  },

  'get /courses/starred': {
    controller: 'CourseController',
    action: 'getStarredCourses'
  },

  'post /paymentgateway/pay': {
    controller: 'SubscriptionController',
    action: 'payWithPaypal'
  },

  'get /mysubscriptions/:userId': {
    controller: 'SubscriptionController',
    action: 'getFromUser'
  },

  'get /paymentgateway/success': {
    controller: 'SubscriptionController',
    action: 'paysuccess'
  },

  'get /paymentgateway/cancel': {
    controller: 'SubscriptionController',
    action: 'paycancel'
  },

  'post /addtonews': {
    controller: 'NewsController',
    action: 'addToNews'
  },

  'post /contactme': {
    controller: 'UserController',
    action: 'contactMe'
  },

  'post /locksubscription': {
    controller: 'SubscriptionController',
    action: 'lockSubscription'
  },

  'post /checkpayment': {
    controller: 'PaymentController',
    action: 'check'
  },

  'post /sendbooking': {
    controller: 'BookingController',
    action: 'createBooking'
  },

  'post /updatebrochure': {
    controller: 'BrochureController',
    action: 'update'
  }
};
