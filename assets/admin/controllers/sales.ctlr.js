angular.module('admin').controller('salesCtlr',
function ($scope, $rootScope, salesService, ExcelService, $timeout) {
  $scope.sales = [];
  $scope.getSales = function () {
    var token = sessionStorage.getItem('Authentication');
    if (token) {
      salesService.getAll()
        .then(function (res) {
          $scope.sales = res.data;
        })
    }
  }
  $scope.getSales();

  $scope.exportToExcel = function () {
    var tableId = '#salesTable';
    var exportHref = ExcelService.tableToExcel(tableId, 'SubscriptionsList');
    $timeout(function () { location.href = exportHref; }, 100); // trigger download
  }
})

.controller('salesDetailsCtlr', function ($scope, $rootScope, $location, salesService, $timeout) {
  $scope.sale = {};

  $scope.getSale = function(id){
    salesService.getSingle(id)
    .then(function (res) {
      if (res && res.data) {
        $scope.sale = res.data;
      }
    })
    .catch(function (err) {
      console.log(err);
    })
  }

  $scope.check = function(param){
    var saleObj = {
      id: $scope.sale.id,
      check: param
    };

    salesService.check(saleObj)
    .then(function(res){
      alert('venta modificada');
      $scope.sale.checked= !$scope.sale.checked;
    })
    .catch(function(err){
      console.log(err);
    })
  }

  //Get Id Param to search Course
  var params = $location.search();

  if (params && params.id) {
    var saleId = params.id;

    //Find course now...
    $scope.getSale(saleId);
  }

  

});
