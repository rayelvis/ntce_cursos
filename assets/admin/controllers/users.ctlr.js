angular.module('admin').controller('usersCtlr',
  function ($scope, $rootScope, userService, ExcelService, $timeout) {
    $scope.users = [];
    $scope.getUsers = function () {
      var token = sessionStorage.getItem('Authentication');
      if (token) {
        userService.getUsers()
          .then(function (res) {
            $scope.users = res.data;
          })
      }
    }
    $scope.getUsers();

    $scope.exportToExcel = function () {
      var tableId = '#usersTable';
      var exportHref = ExcelService.tableToExcel(tableId, 'UsersList');
      $timeout(function () { location.href = exportHref; }, 100); // trigger download
    }
  });
