angular.module('admin').controller('subscriptionsCtlr',
  function ($scope, $rootScope, subscriptionService, ExcelService, $timeout) {
    $scope.subscriptions = [];
    $scope.getSubscriptions = function () {
      var token = sessionStorage.getItem('Authentication');
      if (token) {
        subscriptionService.getAll()
          .then(function (res) {
            $scope.subscriptions = res.data;
          })
      }
    }
    $scope.getSubscriptions();

    $scope.exportToExcel = function () {
      var tableId = '#subscriptionsTable';
      var exportHref = ExcelService.tableToExcel(tableId, 'SubscriptionsList');
      $timeout(function () { location.href = exportHref; }, 100); // trigger download
    }
  })

  .controller('subscriptionsDetailsCtlr', function ($scope, $rootScope, $location, subscriptionService, $timeout) {
    $scope.subscription = {};

    $scope.getSubscription = function(id){
      subscriptionService.getSingle(id)
      .then(function (res) {
        if (res && res.data) {
          $scope.subscription = res.data;
        }
      })
      .catch(function (err) {
        console.log(err);
      })
    }

    $scope.lock = function(param){
      var subscriptionObj = {
        id: $scope.subscription.id,
        lock: param
      };

      subscriptionService.lock(subscriptionObj)
      .then(function(res){
        alert('suscripción modificada');
        $scope.subscription.active = !$scope.subscription.active;
      })
      .catch(function(err){
        console.log(err);
      })
    }

    //Get Id Param to search Course
    var params = $location.search();

    if (params && params.id) {
      var courseId = params.id;

      //Find course now...
      $scope.getSubscription(courseId);
    }

    

  });
