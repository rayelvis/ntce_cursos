angular.module('admin').controller('courseEditCtlr',

  function ($scope, $rootScope, coursesService, $location, instructorService, countryService) {
    //Declare course obj
    $scope.course = {};
    //Search for available instructors
    $scope.instructors = [];

    //Countries from db
    $scope.workCountries = [];

    $scope.getCourse = function (id) {
      //Get course from db
      coursesService.getSingle(id)
        .then(function (res) {
          if (res && res.data) {
            $scope.course = res.data;
            console.log(res.data);
            $scope.course.date = new Date(res.data.date);
          }
        })
        .catch(function (err) {
          console.log(err);
        })
    };

    //Get Id Param to search Course
    var params = $location.search();

    if (params && params.id) {
      var courseId = params.id;

      //Find course now...
      $scope.getCourse(courseId);
    }

    $scope.getInstructors = function () {
      instructorService.getInstructors()
        .then(function (res) {
          console.log(res);
          $scope.instructors = res.data;
        })
        .catch(function (err) {
          console.log(err);
        })
    }

    $scope.getCountries = function () {
      countryService.getCountries()
        .then(function (res) {
          $scope.workCountries = res.data;
        })
        .catch(function (err) {
          console.log(err);
        })
    }

    $scope.submit = function () {
      if ($scope.form.file.$valid && $scope.poster) {
        $scope.update($scope.poster);
      } else {
        $scope.updateNoFile();
      }
    }

    $scope.update = function (poster) {
      coursesService.updateCourse($scope.course, poster)
        .then(function (res) {
          alert(res.message);
          location.href = '/admin/dashboard/#!/';
        })
        .catch(function (err) {
          alert('Error al guardar curso');
        })
    }

    $scope.updateNoFile = function () {
      var p = undefined;
      coursesService.updateCourse($scope.course, p)
        .then(function (res) {
          alert(res.message);
          location.href = '/admin/dashboard/#!/';
        })
        .catch(function (err) {
          alert('Error al guardar curso');
        })
    }

    $scope.getInstructors();
    $scope.getCountries();
  });
