angular.module('admin').controller('coursesCtlr',
  function ($scope, $rootScope, coursesService, ExcelService, $timeout) {
    $scope.courses = [];

    $scope.getCourses = function () {
      var token = sessionStorage.getItem('Authentication');
      if (token) {
        coursesService.getCourses()
          .then(function (res) {
            $scope.courses = res.data;
          })
      }
    }
    $scope.getCourses();

    $scope.exportToExcel = function () {
      var tableId = '#coursessTable';
      var exportHref = ExcelService.tableToExcel(tableId, 'UsersList');
      $timeout(function () { location.href = exportHref; }, 100); // trigger download
    }
  });



//Courses new controller
angular.module('admin').controller('coursesNewCtlr',
  function ($scope, $rootScope, $http, Upload, instructorService, countryService) {


    $scope.now = {};

    //Set now
    $scope.setNow = function(){
      var date = new Date();
      $scope.now = {
        day: date.getDate(),
        month: date.getMonth() + 1,
        year: date.getFullYear()
      }
  
      if($scope.now.month < 10){
        $scope.now.month = '0' + $scope.now.month;
      }
  
      if($scope.now.day < 10){
        $scope.now.day = '0' + $scope.now.day;
      }
    }

    //Buscar instructores disponibles
    $scope.instructors = [];

    //Countries from db
    $scope.workCountries = [];

    //Selected Countries
    $scope.countries = [];

    $scope.getInstructors = function () {
      instructorService.getInstructors()
        .then(function (res) {
          console.log(res);
          $scope.instructors = res.data;
        })
        .catch(function (err) {
          console.log(err);
        })
    }

    $scope.getCountries = function () {
      countryService.getCountries()
        .then(function (res) {
          $scope.workCountries = res.data;
        })
        .catch(function (err) {
          console.log(err);
        })
    }

    $scope.getInstructors();
    $scope.getCountries();
    $scope.setNow();

    //Contructor del modelo
    $scope.course = {
      name: "",
      englishName: "",
      description: "",
      englishDescription: "",
      instructor: "",
      price: 0.0,
      globalPrice: 0.0,
      objetivePublic: "",
      englishObjetivePublic: "",
      activated: false,
      duration: 0.0,
      methodology: "",
      englishMethodology: "",
      resources: "",
      englishResources: "",
      reservations: false,
      contents: "",
      englishContents: "",
      modules: "",
      englishModules: "",
      whatExpect: "",
      englishWhatExpect: "",
      online: false,
      starred: false,
      country: "",
      city: "",
      date: ""
    };

    $scope.setCountry = function (id) {
      var found = false;
      $scope.countries.forEach(function (e, index) {
        if (e === id) {
          $scope.countries.splice(index, 1);
          found = true;
        }
      }, this);

      if (!found) {
        $scope.countries.push(id);
      }
    }

    //Método submit
    $scope.submit = function () {
      if ($scope.form.file.$valid && $scope.poster) {
        $scope.upload($scope.poster);
      }
    };

    $scope.showModel = function(){
      console.log($scope.course);
    }

    $scope.upload = function (poster) {
      Upload.upload({
        url: '/courses/upload',
        arrayKey: '',
        data: {
          name: $scope.course.name,
          englishName: $scope.course.englishName,
          description: $scope.course.description,
          englishDescription: $scope.course.englishDescription,
          instructor: $scope.course.instructor,
          price: $scope.course.price,
          globalPrice: $scope.course.globalPrice,
          objetivePublic: $scope.course.objetivePublic,
          englishObjetivePublic: $scope.course.englishObjetivePublic,
          activated: $scope.course.activated,
          duration: $scope.course.duration,
          methodology: $scope.course.methodology,
          englishMethodology: $scope.course.englishMethodology,
          resources: $scope.course.resources,
          englishResources: $scope.course.englishResources,
          reservations: $scope.course.reservations,
          contents: $scope.course.contents,
          englishContents: $scope.course.englishContents,
          modules: $scope.course.modules,
          whatExpect: $scope.course.whatExpect,
          englishWhatExpect: $scope.course.englishWhatExpect,
          englishModules: $scope.course.englishModules,
          online: $scope.course.online,
          starred: $scope.course.starred,
          country: $scope.course.country,
          city: $scope.course.city,
          date: $scope.course.date
        },
        file: poster
      }).then(function (resp) {
        alert('Curso creado satisfactoriamente');
        location.href = '/admin/dashboard/#!/';
      }, function (resp) {
        console.log('Error status: ' + resp.status);
      });
    }
  });
