angular.module('admin').controller('newsCtlr',
function ($scope, $rootScope, newsService, ExcelService, $timeout) {
  $scope.subscribers = [];
  $scope.getUsers = function () {
    var token = sessionStorage.getItem('Authentication');
    if (token) {
      newsService.getAll()
        .then(function (res) {
          $scope.subscribers = res.data;
        })
    }
  }
  $scope.getUsers();

  $scope.exportToExcel = function () {
    var tableId = '#newsTable';
    var exportHref = ExcelService.tableToExcel(tableId, 'NewslettersList');
    $timeout(function () { location.href = exportHref; }, 100); // trigger download
  }
});
