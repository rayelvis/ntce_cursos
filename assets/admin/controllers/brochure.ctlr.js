angular.module('admin').controller('brochureCtlr',
function ($scope, $rootScope, $timeout, Upload) {
    
    $scope.submit = function () {
        if ($scope.form.file.$valid && $scope.brochure) {
          $scope.upload($scope.brochure);
        }
      };

      $scope.upload = function (brochure) {
        Upload.upload({
          url: '/updatebrochure',
          arrayKey: '',
          file: brochure
        }).then(function (resp) {
          alert(resp.data.message);
          $scope.brochure = {};
        }, function (resp) {
          console.log('Error status: ' + resp.status);
        });
      }
});