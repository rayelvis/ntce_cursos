angular.module('admin').controller('bookingCtlr',
    function ($scope, $rootScope, bookingService, ExcelService, $timeout) {
        $scope.bookings = [];
        $scope.getBookings = function () {
            bookingService.getAll()
                .then(function (res) {
                    $scope.bookings = res.data;
                })
        }
        $scope.getBookings();

        $scope.exportToExcel = function () {
            var tableId = '#bookingsTable';
            var exportHref = ExcelService.tableToExcel(tableId, 'BookingsList');
            $timeout(function () { location.href = exportHref; }, 100); // trigger download
        }
    })

    .controller('bookingDetailsCtlr', function ($scope, $rootScope, $location, bookingService, $timeout) {
        $scope.booking = {};

        $scope.getBooking = function (id) {
            bookingService.getSingle(id)
                .then(function (res) {
                    if (res && res.data) {
                        $scope.booking = res.data;
                    }
                })
                .catch(function (err) {
                    console.log(err);
                })
        }

        //Get Id Param to search Course
        var params = $location.search();

        if (params && params.id) {
            var bookingId = params.id;

            //Find course now...
            $scope.getBooking(bookingId);
        }
    });