angular.module('admin').factory('salesService', function ($q, $http, Upload) {
    function getAll() {
      var deferred = $q.defer();
  
      var token = sessionStorage.getItem('Authentication');
  
      if (token) {
        // send a post request to the server
        $http.get('/api/v1/payment')
          // handle success
          .then(function (res) {
            if ((res.status === 200 || res.status === 201) && res.data) {
              deferred.resolve(res);
            } else {
              deferred.reject(res);
            }
          })
          // handle error
          .catch(function (res) {
            deferred.reject(res);
          });
      }
      // return promise object
      return deferred.promise;
    };
  
    function getSingle(id) {
      var deferred = $q.defer();
  
      // send a post request to the server
      $http.get('/api/v1/payment/' + id)
        // handle success
        .then(function (res) {
          if (res.status === 200 && res.data) {
            //   user = res.data.user;
            deferred.resolve(res);
          } else {
            //   user = undefined;
            deferred.reject(res);
          }
        })
        // handle error
        .catch(function (res) {
          deferred.reject(res);
        });
  
      // return promise object
      return deferred.promise;
    };
  
    function check(saleObj) {
  
      var deferred = $q.defer();
  
      // send a post request to the server
      $http.post('/checkpayment', saleObj)
        // handle success
        .then(function (res) {
          if ((res.status === 200 || res.status === 201) && res.data) {
            //   user = res.data.user;
            deferred.resolve(res);
          } else {
            //   user = undefined;
            deferred.reject(res);
          }
        })
        // handle error
        .catch(function (res) {
          deferred.reject(res);
        });
      // return promise object
      return deferred.promise;
    };
  
  
    // return available functions
    return ({
      getAll: getAll,
      getSingle: getSingle,
      check: check
    });
  })
  