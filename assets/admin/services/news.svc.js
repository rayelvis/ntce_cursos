angular.module('admin').factory('newsService', function ($q, $http, Upload) {
    function getAll() {
      var deferred = $q.defer();
  
      var token = sessionStorage.getItem('Authentication');
  
      if (token) {
        // send a post request to the server
        $http.get('/api/v1/news')
          // handle success
          .then(function (res) {
            if ((res.status === 200 || res.status === 201) && res.data) {
              deferred.resolve(res);
            } else {
              deferred.reject(res);
            }
          })
          // handle error
          .catch(function (res) {
            deferred.reject(res);
          });
      }
      // return promise object
      return deferred.promise;
    };
  
    // return available functions
    return ({
      getAll: getAll
    });
  })
  