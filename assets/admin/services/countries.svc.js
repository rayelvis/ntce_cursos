angular.module('admin').factory('countryService', function ($q, $http) {  
    function getCountries() {
      var deferred = $q.defer();
  
      var token = sessionStorage.getItem('Authentication');
  
      if (token) {
        // send a post request to the server
        $http.get('/api/v1/workcountry')
          // handle success
          .then(function (res) {
            if (res.status === 200 && res.data) {
              deferred.resolve(res);
            } else {
              deferred.reject(res);
            }
          })
          // handle error
          .catch(function (res) {
            deferred.reject(res);
          });
      }
      // return promise object
      return deferred.promise;
    }
    // return available functions
    return ({
      getCountries: getCountries,
    })
  })
  