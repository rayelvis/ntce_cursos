angular.module('admin').factory('bookingService', function ($q, $http) {  
    function getAll() {
      var deferred = $q.defer();
  
      var token = sessionStorage.getItem('Authentication');
  
      if (token) {
        // send a post request to the server
        $http.get('/api/v1/booking')
          // handle success
          .then(function (res) {
            if (res.status === 200 && res.data) {
              deferred.resolve(res);
            } else {
              deferred.reject(res);
            }
          })
          // handle error
          .catch(function (res) {
            deferred.reject(res);
          });
      }
      // return promise object
      return deferred.promise;
    }

    function getSingle(id) {
      var deferred = $q.defer();
  
      // send a post request to the server
      $http.get('/api/v1/booking/' + id)
        // handle success
        .then(function (res) {
          if (res.status === 200 && res.data) {
            //   user = res.data.user;
            deferred.resolve(res);
          } else {
            //   user = undefined;
            deferred.reject(res);
          }
        })
        // handle error
        .catch(function (res) {
          deferred.reject(res);
        });
  
      // return promise object
      return deferred.promise;
    };
    // return available functions
    return ({
      getAll: getAll,
      getSingle: getSingle
    })
  })
  