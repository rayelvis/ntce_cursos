angular.module('admin').factory('coursesService', function ($q, $http, Upload) {
  function getCourses() {
    var deferred = $q.defer();

    var token = sessionStorage.getItem('Authentication');

    if (token) {
      // send a post request to the server
      $http.get('/api/v1/course')
        // handle success
        .then(function (res) {
          if (res.status === 200 && res.data) {
            deferred.resolve(res);
          } else {
            deferred.reject(res);
          }
        })
        // handle error
        .catch(function (res) {
          deferred.reject(res);
        });
    }
    // return promise object
    return deferred.promise;
  };

  function getSingle(id) {
    var deferred = $q.defer();

    // send a post request to the server
    $http.get('/api/v1/course/' + id)
      // handle success
      .then(function (res) {
        if (res.status === 200 && res.data) {
          //   user = res.data.user;
          deferred.resolve(res);
        } else {
          //   user = undefined;
          deferred.reject(res);
        }
      })
      // handle error
      .catch(function (res) {
        deferred.reject(res);
      });

    // return promise object
    return deferred.promise;
  }

  function updateCourse(course, poster) {
    var deferred = $q.defer();
    debugger;

    if (poster !== undefined) {
      Upload.upload({
        url: '/courses/edit',
        arrayKey: '',
        data: {
          name: course.name,
          englishName: course.englishName,
          description: course.description,
          englishDescription: course.englishDescription,
          instructor: course.instructor,
          price: course.price,
          globalPrice: course.globalPrice,
          objetivePublic: course.objetivePublic,
          englishObjetivePublic: course.englishObjetivePublic,
          activated: course.activated,
          duration: course.duration,
          methodology: course.methodology,
          englishMethodology: course.englishMethodology,
          resources: course.resources,
          englishResources: course.englishResources,
          reservations: course.reservations,
          contents: course.contents,
          englishContents: course.englishContents,
          modules: course.modules,
          englishModules: course.englishModules,
          whatExpect: course.whatExpect,
          englishWhatExpect: course.englishWhatExpect,
          online: course.online,
          starred: course.starred,
          country: course.country,
          city: course.city,
          date: course.date,
          id: course.id
        },
        file: poster
      }).then(function (resp) {
        if (resp.status === 200 && resp.data) {
          //   user = res.data.user;
          deferred.resolve(resp);
        } else {
          //   user = undefined;
          deferred.reject(resp);
        }

      }, function (resp) {
        deferred.reject(resp);
      });

      // return promise object
      return deferred.promise;
    } else {
      Upload.upload({
        url: '/courses/edit',
        arrayKey: '',
        data: {
          name: course.name,
          englishName: course.englishName,
          description: course.description,
          englishDescription: course.englishDescription,
          instructor: course.instructor,
          price: course.price,
          globalPrice: course.globalPrice,
          objetivePublic: course.objetivePublic,
          englishObjetivePublic: course.englishObjetivePublic,
          activated: course.activated,
          duration: course.duration,
          methodology: course.methodology,
          englishMethodology: course.englishMethodology,
          resources: course.resources,
          englishResources: course.englishResources,
          reservations: course.reservations,
          contents: course.contents,
          englishContents: course.englishContents,
          modules: course.modules,
          englishModules: course.englishModules,
          whatExpect: course.whatExpect,
          englishWhatExpect: course.englishWhatExpect,
          online: course.online,
          starred: course.starred,
          country: course.country,
          city: course.city,
          date: course.date,
          id: course.id
        }
      }).then(function (resp) {
        if (resp.status === 200 && resp.data) {
          //   user = res.data.user;
          deferred.resolve(resp);
        } else {
          //   user = undefined;
          deferred.reject(resp);
        }

      }, function (resp) {
        deferred.reject(resp);
      });

      // return promise object
      return deferred.promise;
    }
    
  }

  // return available functions
  return ({
    getCourses: getCourses,
    getSingle: getSingle,
    updateCourse: updateCourse
  });
})
