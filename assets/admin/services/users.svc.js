angular.module('admin').factory('userService', function ($q, $http) {
  var user = null;

  function getUsers() {
    var deferred = $q.defer();

    var token = sessionStorage.getItem('Authentication');

    if (token) {
      // send a post request to the server
      $http.get('/api/v1/user')
        // handle success
        .then(function (res) {
          if (res.status === 200 && res.data) {
            deferred.resolve(res);
          } else {
            deferred.reject(res);
          }
        })
        // handle error
        .catch(function (res) {
          deferred.reject(res);
        });
    }
    // return promise object
    return deferred.promise;
  }
  // return available functions
  return ({
    getUsers: getUsers,
  })
})
