'use strict';

var admin = angular.module('admin',
['ngRoute',
'ui.bootstrap',
'siTable',
'ngFileUpload',
'textAngular'
])

admin.config(
    function ($routeProvider, $locationProvider, $httpProvider) {
      $routeProvider
        .when('/admin', {
          templateUrl: '/admin/templates/dashboard.html',
          controller: 'dashboardCtrl'
        })
        .when('/logout',{
          templateUrl: '/templates/auth/logout.html',
          controller: 'logoutCtlr'
        })
        .when('/users', {
          templateUrl: '/admin/templates/users.list.html',
          controller: 'usersCtlr'
        })
        .when('/courses', {
          templateUrl: '/admin/templates/courses.list.html',
          controller: 'coursesCtlr'
        })
        .when('/courses/new', {
          templateUrl: '/admin/templates/courses.new.html',
          controller: 'coursesNewCtlr'
        })
        .when('/courses/edit/:id', {
          templateUrl: '/admin/templates/courses.edit.html',
          controller: 'courseEditCtlr'
        })
        .when('/news', {
          templateUrl: '/admin/templates/newsletters.list.html',
          controller: 'newsCtlr'
        })
        .when('/subscriptions', {
          templateUrl: '/admin/templates/subscriptions.list.html',
          controller: 'subscriptionsCtlr'
        })
        .when('/subscriptions/:id', {
          templateUrl: '/admin/templates/subscriptions.detail.html',
          controller: 'subscriptionsDetailsCtlr'
        })
        .when('/sales', {
          templateUrl: '/admin/templates/sales.list.html',
          controller: 'salesCtlr'
        })
        .when('/sales/:id', {
          templateUrl: '/admin/templates/sales.detail.html',
          controller: 'salesDetailsCtlr'
        })
        .when('/bookings', {
          templateUrl: '/admin/templates/booking.list.html',
          controller: 'bookingCtlr'
        })
        .when('/bookings/:id', {
          templateUrl: '/admin/templates/booking.detail.html',
          controller: 'bookingDetailsCtlr'
        })
        .when('/brochure', {
          templateUrl: '/admin/templates/brochure.html',
          controller: 'brochureCtlr'
        })
        .otherwise({
          redirectTo: '/admin',
          caseInsensitiveMatch: true
        });

      $locationProvider
        .html5Mode(false)
        .hashPrefix('!');

        var tk = sessionStorage.getItem('Authentication');
        if(tk){
          $httpProvider.interceptors.push(function() {
            return {
             request: function(config) {
                  config.headers['Authorization'] = tk;
                  return config;
              }
            };
          });
        }
  });

  admin.run(['$rootScope', function($rootScope){
    $rootScope.lang = 'en';
  }])

  admin.controller('dashboardCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {

  }]);
