'use strict';

var ntce = angular.module('ntce', ['ngRoute',
  'ui.bootstrap',
  'pascalprecht.translate',
  'ngCookies'
]);

ntce.config(
  function ($routeProvider, $locationProvider, $translateProvider, $httpProvider) {
    $routeProvider
      .when('/', {
        templateUrl: '/templates/home.html',
        controller: 'homeCtrl'
      })
      .when("/signin", {
        templateUrl: '/templates/auth/signin.html',
        controller: 'loginCtlr'
      })
      .when("/restorepass", {
        templateUrl: '/templates/auth/restore-pass.html',
        controller: 'restorePassCtlr'
      })
      .when("/signup", {
        templateUrl: '/templates/auth/signup.html',
        controller: 'registerCtlr'
      })
      .when("/unactivated", {
        templateUrl: '/templates/auth/please-activate.html',
        controller: 'unactivatedCtlr'
      })
      .when("/logout", {
        templateUrl: '/templates/auth/logout.html',
        controller: 'logoutCtlr'
      })
      .when("/newpassword", {
        templateUrl: '/templates/auth/new-password.html',
        controller: 'restorePassCtlr'
      })
      .when("/myaccount", {
        templateUrl: '/templates/user/myaccount.html',
        controller: 'myAccountCtlr'
      })
      .when("/courses", {
        templateUrl: 'templates/courses/courses.list.html',
        controller: 'coursesListCtlr'
      })
      .when("/about", {
        templateUrl: 'templates/courses/about.html',
        controller: 'aboutCtlr'
      })
      .when("/engineering", {
        templateUrl: 'templates/courses/engineering.html',
        controller: 'engineeringCtlr'
      })
      .when("/course/detail/:id", {
        templateUrl: 'templates/courses/detailcourse.html',
        controller: 'detailcourseCtlr'
      })
      .when("/contact", {
        templateUrl: 'templates/courses/contact.html',
        controller: 'contactCtlr'
      })
      .when("/cart", {
        templateUrl: 'templates/courses/car.html',
        controller: 'cartCtlr'
      })
      .when("/checkout", {
        templateUrl: 'templates/courses/checkout.html',
        controller: 'checkoutCtlr'
      })
      .when("/404", {
        templateUrl: 'templates/courses/404.html',
        controller: '404Ctlr'
      })
      .when("/500", {
        templateUrl: 'templates/courses/500.html',
        controller: '500Ctlr'
      })
      .when("/subscription/detail/:id", {
        templateUrl: 'templates/courses/subscriptiondetail.html',
        controller: 'subscriptionDetailCtlr'
      })
      .when("/sale/success", {
        templateUrl: 'templates/courses/sale.success.html'
      })
      .otherwise({
        redirectTo: '/',
        caseInsensitiveMatch: true
      });

    $locationProvider
      .html5Mode(false)
      .hashPrefix('')

    $translateProvider
      .useStaticFilesLoader({
        prefix: '/translations/',
        suffix: '.json'
      })
      .preferredLanguage('es')
      .useLocalStorage()
      .useMissingTranslationHandlerLog();

    var tk = sessionStorage.getItem('Authentication');
    if (tk) {
      $httpProvider.interceptors.push(function () {
        return {
          'request': function (config) {
            config.headers['Authorization'] = tk;
            return config;
          }
        };
      });
    }
  });

ntce.run(['$rootScope', function ($rootScope, $httpProvider, $location) {
  $rootScope.lang = 'es';
}])

ntce.controller('homeCtrl', ['$scope', '$rootScope', 'starredCoursesService', function ($scope, $rootScope, starredCoursesService) {
  $rootScope.currentUser = $rootScope.user || {};

  $rootScope.starreds = [];

  $scope.getStarredCourses = function () {
    starredCoursesService.getStarred()
      .then(function (res) {
        if (res.status === 200 && res.data && res.data.courses) {
          $rootScope.starreds = res.data.courses;
        }
      })
      .catch(function (err) {
        console.log(err);
      })
  }

  //Buscar cursos marcados como novedades
  $scope.getStarredCourses();
}])

ntce.controller('mainCtlr', function ($scope, $rootScope, authService) {
  //Agregar Cursos al carrito
  $rootScope.cart = [];
  $rootScope.courses = [];
  $rootScope.sessionUser = {};

  $rootScope.addToCart = function (course) {

    //Verify session before add items to cart
    var verifySession = function(){
      return authService.isLoggedIn()
      .then(function(res){
        console.log(res);

        //Add to cart if they have correr session
        var found = false;
        $rootScope.cart.forEach(element => {
          if(element.id === course.id){
            alert('Ya has seleccionado este curso, puedes cambiar la cantidad que deseas comprar en el carrito de compras.')
            found = true;
            return;
          }
        });
        if(!found){
          course.qty = 1;
          $rootScope.cart.push(course);
          $("#addToCart").modal("show");
        }  
      }).catch(function(err){
        $("#noLogged").modal("show");
      });
    }

    var isLogged = verifySession();  
  }
});
