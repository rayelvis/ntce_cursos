angular.module('ntce').component('ntceBanner', {
  templateUrl: '../../templates/partials/banner.html',
  controller: bannerController,
  bindings: {
    imageurl: '@',
    color: '@',
    flag1: '@',
    flag2: '@',
    flag3: '@',
    content: '@',
    icon1: '@',
    icon2: '@',
    icon3: '@',
    course: '@',
    courseenglish: '@',
    lang: '@',
    method: '@',
    timer: '@'
  }
})

bannerController.$inject = ['$scope', '$rootScope', '$http', 'courseService', 'newsService']
function bannerController(scope, rootScope, http, courseService, newsService) {
  var _this = this;
  _this.countries = [];

  getCountries = function () {
    http.get('/api/v1/workcountry')
      // handle success
      .then(function (res) {
        if (res.status === 200 && res.data) {
          _this.countries = res.data;
        } else {
          console.log(res);
        }
      })
      // handle error
      .catch(function (res) {
        console.log(res);
      });
  }

  getCountries();

  

  scope.search = function (f) {
    courseService.getByFilters(f)
      .then(function (res) {
        if (res.status === 200 && res.data) {
          rootScope.courses = res.data.courses;
        }
        else {
          console.log(res);
        }
      })
      .catch(function (err) {
        console.log(err);
      })
  }

  scope.setTimer = function () {

    var position = 1;

    var images = [
      '/images/banner/banner_1.jpeg',
      '/images/banner/banner_2.jpeg',
      '/images/banner/banner_3.jpeg'
    ];

    var timer = $('#timer');
    if(!window.timer){
      window.timer = timer;
      window.setInterval(function () {
        $('#timer').stop().animate({ opacity: 0.9 }, 300, function () {
          $('#timer').css({ 'background-image': 'url(' + images[position] + ')' })
            .animate({ opacity: 1 }, { duration: 300 });
          if (position === 2) {
            position = 0;
          } else {
            position++;
          }
        });
      }, 5000);
    }    
  }

  scope.setTimer();
}
