angular.module('ntce').component('bodyParser', {
  transclude: true,
  template: '<div class="parser"></div>',
  controller: function ($scope, $element, $compile) {
    var self = this;
    this.$onChanges = function (changesObj) {
      if (changesObj.content) {
        if (changesObj.content.currentValue) {
          if (changesObj.content.currentValue.length > 0) {
            var newElement = $compile(changesObj.content.currentValue)($scope);
            $element.html(newElement);
          }
        }
      }
    }

  },
  bindings: {
    content: '@',
  }
})
