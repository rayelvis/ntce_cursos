angular.module('ntce').controller('registerCtlr',
  function ($scope, $location, authService) {

    $scope.person = {};
    $scope.errorMessage = '';

    $scope.doSignUp = function () {
      // initial values
      $scope.error = false;
      $scope.disabled = true;

      if(!$scope.validateForm()){
        $scope.error = "Verify password and password confirm field";
        return;
      }
      var newUser = Object.assign({}, $scope.person);
      delete newUser.passwordConfirm;
      // call register from service
      authService.signup($scope.person)
        // handle success
        .then(function (res) {
          $location.path('/unactivated');
          $scope.disabled = false;
          $scope.registerForm = {};
        })
        // handle error
        .catch(function (err) {
          $scope.errorMessage = "Something went wrong!"; 
          $scope.error = true;
          $scope.disabled = false;         
          if(err.data && err.data.errorCode){
            $scope.errorMessage = err.data.message;
            debugger;
            return;
          }
          
        });

    };

    $scope.validateForm = function(){
      if($scope.person.passwordConfirm != $scope.person.password){
        return false;
      }

      if($scope.person.password.length < 8){
        return false;
      }
      debugger;
      return true;
    }

  });
