angular.module('ntce').controller('logoutCtlr',

  function ($scope, $location, authService, $timeout) {
    $scope.isLogged = false;
    authService.logout()
      .then(function (res) {
        $timeout(function () {
          $location.path('/');
        }, 5000);
      })
      // handle error
      .catch(function (res) {
        console.log(res);
      });
  });
