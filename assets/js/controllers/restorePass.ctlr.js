angular.module('ntce').controller('restorePassCtlr',

  function ($scope, authService, $timeout, $routeParams) {
    $scope.person = {};

    $scope.doRestore = function () {
      // call login from service
      authService.resetPass($scope.person)
        // handle success
        .then(function (res) {
          alert('Request recived, please check your email and follow the reset password link');
          console.log(res);
        })
        // handle error
        .catch(function (res) {
          alert('Request recived, please check your email and follow the reset password link');
          console.log(res);
        });
    };

    $scope.savePass = function () {
      if (!$scope.validateForm()) {
        return;
      }

      if ($routeParams.token)
        $scope.person.token = $routeParams.token;
      else
        $location.path('/signin');

      var newUser = Object.assign({}, $scope.person);
      delete newUser.passwordConfirm;

      authService.newpass(newUser)
        // handle success
        .then(function () {
          $scope.message = 'Password changed successfully';
          alert('Password changed successfully');
          window.location.href = '#/signin';
        })
        // handle error
        .catch(function () {
          $scope.message = "Something went wrong!";
        });
    }

    $scope.validateForm = function () {
      if ($scope.person.passwordConfirm != $scope.person.password) {
        return false;
      }

      if ($scope.person.password.length < 8) {
        return false;
      }
      return true;
    }
  });
