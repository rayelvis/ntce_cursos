angular.module('ntce').controller('newsCtlr',

  function ($scope, $rootScope, newsService) {
    $scope.subscriber = {};

    $scope.addToNews = function(){
        newsService.add(this.subscriber)
        .then(function(res){
            if(res.data && res.data.code === 'CREATED'){
                var lang = $rootScope.lang;
                var message = "Newsletters subscription success, Thanks for choosing us!"; 
                if(lang === 'es')
                   message = "Suscripción a newsletters exitosa, ¡gracias por preferirnos!";
                alert(message);
                $scope.subscriber = {};
            }
        })
        .catch(function(err){
            console.log(err);
            if(err && err.data && err.data.invalidAttributes && err.data.invalidAttributes.emailAddress){
                if(err.data.invalidAttributes.emailAddress && err.data.invalidAttributes.emailAddress[0].rule){
                    if(err.data.invalidAttributes.emailAddress[0].rule === 'unique'){
                        var lang = $rootScope.lang;
                        var message = "Your email address has subscribed to our newsletters!"; 
                        if(lang === 'es')
                           message = "Tu correo ya se encuentra suscrito a nuestras newsletters!";
                        alert(message);
                        $scope.subscriber = {};
                    }
                }
            }
            
        })
    
      }
  });
