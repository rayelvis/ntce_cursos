angular.module('ntce').controller('cartCtlr',

  function ($scope, $rootScope, $location) {
    console.log($rootScope.cart);
    $scope.cartChange = 0;

    $scope.totalPrice = 0;

    $scope.calcTotal = function () {
      if ($rootScope.cart) {
        $scope.totalPrice = 0;
        $rootScope.cart.forEach(function (course) {
          var courseTotal = course.globalPrice * course.qty;
          $scope.totalPrice = $scope.totalPrice + courseTotal;
        }, this);
      }
    }

    $scope.qtys = [
      {name: 1},
      {name: 2},
      {name: 3},
      {name: 4},
      {name: 5},
      {name: 6},
      {name: 7},
      {name: 8},
      {name: 9},
      {name: 10},
      {name: 11},
      {name: 12},
      {name: 13},
      {name: 14},
      {name: 15},
    ];

    $scope.changeQty = function(i, q){
      debugger;
        //Modify Qty
        $rootScope.cart[i].qty = q.cartChange;
        console.log($rootScope.cart);


        //Recalculate total
        $scope.calcTotal();
    }

    $scope.remove = function (i) {
      console.log(i);
      $rootScope.cart.splice(i,1);
       $scope.calcTotal();

    }

    $scope.checkOut = function(){
      $location.path('/checkout');
    }

    //Call functions stack
    $scope.calcTotal();
  });
