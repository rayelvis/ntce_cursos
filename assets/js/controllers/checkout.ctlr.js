angular.module('ntce').controller('checkoutCtlr',

  function ($scope, $rootScope, $location, authService) {

    if($rootScope.cart.length === 0){
      window.location.href = '/#/';
    }
    $scope.cartChange = 0;
    $scope.currentUser = {};

    $scope.totalPrice = 0;

    $scope.setUser = function(){
      authService.isLoggedIn()
      .then(function(res){
          $scope.currentUser = res.data.user;
      })
      .catch(function(){
        alert('Session error, your payment can not be executed');
        window.location.href = "/#/signin";
      })
    }

    $scope.setUser();

    $scope.calcTotal = function () {
      if ($rootScope.cart) {
        $scope.totalPrice = 0;
        $rootScope.cart.forEach(function (course) {
          var courseTotal = course.globalPrice * course.qty;
          $scope.totalPrice = $scope.totalPrice + courseTotal;
        }, this);
      }
    }

    $scope.changeQty = function (i, q) {
      //Modify Qty
      $rootScope.cart[i].qty = q.cartChange;
      console.log('Qty modified');


      //Recalculate total
      $scope.calcTotal();
    }

    $scope.remove = function (i) {
      console.log(i);
      $rootScope.cart.splice(i, 1);
      $scope.calcTotal();

    }

    $scope.checkOut = function () {
      $location.path('/checkout');
    }

    //Call functions stack
    $scope.calcTotal();
  });
