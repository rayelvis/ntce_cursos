angular.module('ntce').controller('LanguageSwitchCtlr',

function ($scope, $rootScope, $translate) {
  $scope.changeLanguage = function(langKey) {
    $translate.use(langKey);
  };

  $rootScope.$on('$translateChangeSuccess', function(event, data) {
    var language = data.language;
    $rootScope.lang = language;
  });

  //Verify if prefered lang is selected and stored in localStorage when controller run
  var currentLang = localStorage.getItem('NG_TRANSLATE_LANG_KEY');
  if(currentLang){
    $rootScope.lang = currentLang;
    $scope.changeLanguage(currentLang);
  }
});
