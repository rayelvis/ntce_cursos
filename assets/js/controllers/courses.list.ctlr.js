angular.module('ntce').controller('coursesListCtlr',
  function ($scope, $rootScope, courseService) {

    $scope.getAllCourses = function () {
      courseService.getAll()
        .then(function (res) {
          $rootScope.courses = res.data;
          console.log(res.data);
        })
        .catch(function (err) {
          console.log('Ha ocurrido un error al buscar cursos');
        });
    }

    $scope.getAllCourses();
  });
