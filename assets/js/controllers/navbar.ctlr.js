angular.module('ntce').controller('navbarCtlr',

  function ($scope, $location, $timeout, authService, $rootScope, $http) {
    $scope.roleUserLoggued = $rootScope.roleUserLoggued || false; 
    debugger;
    authService.isLoggedIn()
      .then(function (data) {
        $scope.isLogged = false;
        $rootScope.currentUser = authService.getUser() || {};
        if ($scope.currentUser)
          $scope.isLogged = true;
      }).catch(function (err) {
        console.log(err);
      });

      $scope.goToDashboard = function(){
        $http.get('/admin')
        .then(function(res){
          console.log(res);
          if(res.status === 200){
            window.location.href = '/admin/dashboard';
          }
        })
        .catch(function(err){
          console.log(err);
        })
      }
  });
