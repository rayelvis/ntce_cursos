angular.module('ntce').controller('bookingCtlr',

  function ($scope, $rootScope, bookingService, $timeout) {
    $scope.booking = {};
    $scope.sending = false;

    $rootScope.sendBooking= function(){
        if($scope.sending)
            return;

        var booking = $scope.booking;
        booking.course = $scope.course.id;

        $scope.sending = true;
        bookingService.add(booking)
        .then(function(res){
            if(res.data && res.data.code === 'CREATED'){
                var lang = $rootScope.lang;
                var message = "Reserve sended, our team will contact you to give you more details."; 
                if(lang === 'es')
                   message = "Reserva enviada, nuestro equipo te contactará para darte mayores detalles.";
                alert(message);
                $scope.booking = {};
                $scope.sending = false;
            }
        })
        .catch(function(err){
            console.log(err);   
            $scope.sending = false;         
        })
      }
  });
