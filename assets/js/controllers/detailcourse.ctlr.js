angular.module('ntce').controller('detailcourseCtlr',

  function ($scope, $rootScope, $location, courseService, dateService, $timeout, $http) {
    var params = $location.search();
    $scope.course = {};
    $scope.langa = $rootScope.lang;
    if (params.id) {
      var courseId = params.id;

      //Search course in db
      courseService.getSingle(courseId)
        .then(function (res) {
          if (res.data) {
            $scope.course = res.data;

            $http.get('/api/v1/user').then(function (res) {
              if (res.data) {
                $scope.users = res.data;

                $scope.course.comments.forEach(function (el) {
                  $scope.users.forEach(function (us) {
                    if (el.user === us.id) {
                      el.userName = us.name;
                    }
                  });
                });
                //Parse date format
                $scope.course.rating.onePercent = $scope.course.rating.oneBase / $scope.course.rating.totalCommentsBase * 100;
                $scope.course.rating.twoPercent = $scope.course.rating.twoBase / $scope.course.rating.totalCommentsBase * 100;
                $scope.course.rating.treePercent = $scope.course.rating.treeBase / $scope.course.rating.totalCommentsBase * 100;
                $scope.course.rating.fourPercent = $scope.course.rating.fourBase / $scope.course.rating.totalCommentsBase * 100;
                $scope.course.rating.fivePercent = $scope.course.rating.fiveBase / $scope.course.rating.totalCommentsBase * 100;
                $scope.course.date = dateService.getFormatedDate($scope.course.date);
                $scope.course.method = (res.data.online ? "Online" : res.data.city + ", " + $scope.course.date);
              }
            })
              .catch(function (err) {

              });

          }
        })
        .catch(function (err) {
          alert('Ha ocurrido un error al buscar este curso');
          window.location.href = '#/500';
        })

    } else {
      window.location.href = '#/404';
    }

    //Cart operations

    //Add
    $scope.addToCart = function (c) {
      c.qty = 1;
      $rootScope.addToCart(c);
    }


    //Login required
    $scope.goTo = function (path) {
      $("#noLogged").modal("hide");
      $timeout(function () {
        window.location.href = path;
      }, 1000)
    }

    //goToCart

    $("#goToCart").click(function () {
      $("#addToCart").modal("hide");
      $timeout(function () {
        window.location.href = "#/cart";
      }, 1000)

    });
    // $("#addToCart").on('hidden.bs.modal', function () {

    // });







  });
