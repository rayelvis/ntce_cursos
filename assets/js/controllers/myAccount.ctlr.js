angular.module('ntce').controller('myAccountCtlr',

    function ($scope, $timeout, authService, $rootScope, $http, accountService, courseService, commentService) {
        $scope.edit = false;
        $scope.valoration = {
            value: "5"
        };

        $scope.currentCourse = 0;

        authService.isLoggedIn()
            .then(function (data) {
                $scope.isLogged = false;
                if ($scope.currentUser) {
                    $scope.isLogged = true;
                    $scope.currentUser = authService.getUser() || {};
                }
            }).catch(function (err) {
                console.log(err);
            });

        accountService.getSubscriptions()
            .then(function (res) {
                $scope.subscriptions = res.data.obj[0].subscriptions;
                //Map Courses
                courseService.getAll()
                    .then(function (res2) {
                        $scope.subscriptions.forEach(function (val) {
                            res2.data.forEach(function (c) {
                                if (c.id === val.course)
                                    val.courseData = {
                                        name: c.name,
                                        englishName: c.englishName,
                                        poster: c.poster,
                                        online: c.online,
                                        instructor: c.instructor.name,
                                        city: c.city,
                                        date: c.date
                                    };
                            });
                        });
                    }).catch(function (err2) {
                        console.log(err2);
                    });

            }).catch(function (err) {
                console.log(err);
            });

        $scope.ModalShow = function (c) {
            $('.modalComment').modal('show');
            $scope.currentCourse = c;
        };


        $scope.showCourse = function (id) {
            var path = "#/course/detail/path?id=" + id;
            window.location.href = path;
        };

        $scope.setValoration = function (val) {
            $scope.valoration.value = val;
        };

        $scope.saveValoration = function () {
            var obj = {
                value: $scope.valoration.value,
                text: $scope.valoration.text,
                user: $scope.currentUser.id,
                course: $scope.currentCourse
            };

            commentService.create(obj)
                .then(function (res) {
                    $('.modalComment').modal('hide');
                    alert('Course created');
                })
                .catch(function (err) {
                    console.log(err);
                });

        };
    });
