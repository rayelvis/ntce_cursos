angular.module('ntce').controller('unactivatedCtlr',

function ($scope, $location, $timeout) {
    $scope.time = 6;
    function updateReloj()
    {
        if($scope.time === 0)
        {
            $location.path('/signin');
        }else{
            $scope.time -= 1;
            $timeout(updateReloj,1000);
        }
    }
    updateReloj();
});
