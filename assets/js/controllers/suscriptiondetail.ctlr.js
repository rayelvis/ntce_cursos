angular.module('ntce').controller('subscriptionDetailCtlr',

  function ($scope, $rootScope, $location, courseService, dateService) {
    var params = $location.search();
    $scope.course = {};
    $scope.langa = $rootScope.lang;
    if (params.id) {
      var courseId = params.id;

      //Search course in db
      courseService.getSingle(courseId)
        .then(function (res) {
          if (res.data) {
            $scope.course = res.data;
            //Parse date format
            debugger;
            $scope.course.date = dateService.getFormatedDate($scope.course.date);
            $scope.course.method = (res.data.online ? "Online" : res.data.city + ", " + $scope.course.date);
            debugger;
          }
        })
        .catch(function (err) {
          alert('Ha ocurrido un error al buscar este curso');
          window.location.href = '#/500';
        })

    } else {
      //window.location.href = '#/404';
    }

    //Cart operations

    //Add
    $scope.addToCart = function(c){
        $rootScope.addToCart(c);
    }
  });
