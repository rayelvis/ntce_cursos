angular.module('ntce').controller('loginCtlr',
  function ($scope, $rootScope, $location, authService, $routeParams, $timeout, $location) {
    $scope.person = {};
    $scope.errorMessage = '';

    $scope.SetError = function(message){
      $scope.errorMessage = message;
    }
    //If mail is a valid parameter set to person empty object
    if ($routeParams.email)
      $scope.person.email = $routeParams.email;

    if ($routeParams.activated)
      $scope.activated = $routeParams.activated;

    $scope.doSignIn = function () {
      // call login from service
      authService.signin($scope.person.email, $scope.person.password)
        // handle success
        .then(function (res) {

          if(res.data && res.data.errorCode){
            $scope.SetError(res.data.message);
            $scope.person.password = '';
          }

          if (res && res.data && res.data.token){
            sessionStorage.setItem('Authentication', 'JWT ' + res.data.token);
            $rootScope.sessionUser = res.data.user;
            $rootScope.roleUserLoggued = res.data.user.role == 'admin' ? true : false;
            $location.path('/');
          }
            
        })
        // handle error
        .catch(function (res) {
          $scope.errorMessage = res.data.message;
          $scope.person.password = '';
          sessionStorage.removeItem('Authentication');
          if (res.data && res.data.code && res.data.code === 'E_UNACTIVATED_ACCOUNT') {
            $location.path('/unactivated');
          }

        });
    };
  });
