angular.module('ntce').factory('dateService', function ($q, $timeout, $http) {
  function getFormatedDate(d) {
    var date = new Date(d);
    var month = date.getUTCMonth() + 1; //months from 1-12
    var day = date.getUTCDate();
    var year = date.getUTCFullYear();
    return year + "/" + month + "/" + day;
  }

  return ({
    getFormatedDate: getFormatedDate
  })
})
