angular.module('ntce').factory('courseService', function ($q, $timeout, $http) {
  function getAll() {
    var deferred = $q.defer();

    // send a post request to the server
    $http.get('/api/v1/course')
      // handle success
      .then(function (res) {
        if (res.status === 200 && res.data) {
          //   user = res.data.user;
          deferred.resolve(res);
        } else {
          //   user = undefined;
          deferred.reject(res);
        }
      })
      // handle error
      .catch(function (res) {
        deferred.reject(res);
      });

    // return promise object
    return deferred.promise;
  }

  function getByFilters(filters){
    var deferred = $q.defer();
    
        var prepFilters = '?';
        if(filters.name && filters.name !== '')
          prepFilters += 'name=' + filters.name + '&';

        if(filters.online){
          prepFilters += 'online=' + filters.online + '&';
        }

        if(filters.country){
          prepFilters += 'country=' + filters.country;
        }

        // send a post request to the server
        $http.get('/courses/byfilters/' + prepFilters)
          // handle success
          .then(function (res) {
            if (res.status === 200 && res.data) {
              //   user = res.data.user;
              deferred.resolve(res);
            } else {
              //   user = undefined;
              deferred.reject(res);
            }
          })
          // handle error
          .catch(function (res) {
            deferred.reject(res);
          });
    
        // return promise object
        return deferred.promise;
  }

  function getSingle(id) {
    var deferred = $q.defer();

    // send a post request to the server
    $http.get('/api/v1/course/' + id)
      // handle success
      .then(function (res) {
        if (res.status === 200 && res.data) {
          //   user = res.data.user;
          console.log(res.data);
          deferred.resolve(res);
        } else {
          //   user = undefined;
          deferred.reject(res);
        }
      })
      // handle error
      .catch(function (res) {
        deferred.reject(res);
      });

    // return promise object
    return deferred.promise;
  }

  // return available functions
  return ({
    getAll: getAll,
    getSingle: getSingle,
    getByFilters: getByFilters
  })
})
