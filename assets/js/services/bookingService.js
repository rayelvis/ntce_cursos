angular.module('ntce').factory('bookingService', function ($q, $http) {
    function add(booking) {
        var deferred = $q.defer();
        // send a post request to the server
        $http.post('sendbooking', booking)
            // handle success
            .then(function (res) {
                if ((res.status === 200 || res.status === 201) && res.data) {
                    //   user = res.data.user;
                    deferred.resolve(res);
                } else {
                    //   user = undefined;
                    deferred.reject(res);
                }
            })
            // handle error
            .catch(function (res) {
                deferred.reject(res);
            });
        // return promise object
        return deferred.promise;
    }

    // return available functions
    return ({
        add: add
    })
})
