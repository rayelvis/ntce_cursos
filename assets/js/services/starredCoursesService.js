angular.module('ntce').factory('starredCoursesService', function ($q, $timeout, $http) {
    function getStarred() {
      var deferred = $q.defer();
  
      // send a post request to the server
      $http.get('/courses/starred')
        // handle success
        .then(function (res) {
          if (res.status === 200 && res.data) {
            deferred.resolve(res);
          } else {
            deferred.reject(res);
          }
        })
        // handle error
        .catch(function (res) {
          deferred.reject(res);
        });
  
      // return promise object
      return deferred.promise;
    }
  
    // return available functions
    return ({
      getStarred: getStarred
    })
  })
  