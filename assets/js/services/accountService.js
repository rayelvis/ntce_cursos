angular.module('ntce').factory('accountService', function ($q, $timeout, $http, $rootScope, authService) {
    var user = null;

    function getSubscriptions(info) {
        var deferred = $q.defer();

        var token = sessionStorage.getItem('Authentication');

        if (token) {

            authService.isLoggedIn()
                .then(function (data) {
                    var user = authService.getUser();

                    if (user) {
                        // send a post request to the server
                        $http.get('/mysubscriptions/' + user.id, info)
                            // handle success
                            .then(function (res) {
                                if (res.status === 200 && res.data) {
                                    deferred.resolve(res);
                                } else {
                                    deferred.reject(res);
                                }
                            })
                            // handle error
                            .catch(function (res) {
                                deferred.reject(res);
                            });
                    }
                }).catch(function (err) {
                    console.log(err);
                });

        } else {
            deferred.reject({ data: { message: 'No session', status: false } });
        }
        // return promise object
        return deferred.promise;
    }

    // return available functions
    return ({
        getSubscriptions: getSubscriptions,
    })
})
