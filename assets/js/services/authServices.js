angular.module('ntce').factory('authService', function ($q, $timeout, $http, $rootScope) {
  var user = null;

  function isLoggedIn() {
    var deferred = $q.defer();

    var token = sessionStorage.getItem('Authentication');

    if (token) {
      // send a post request to the server
      $http.get('/isauth')
        // handle success
        .then(function (res) {
          if (res.status === 200 && res.data.user) {
            user = res.data.user;
            user.token = token;
            deferred.resolve(res);
          } else {
            user = undefined;
            deferred.reject(res);
          }
        })
        // handle error
        .catch(function (res) {
          user = undefined;
          sessionStorage.removeItem('Authorization');
          deferred.reject(res);
        });
    }else{
      deferred.reject({data: {message: 'No session', status: false}});
    }
    // return promise object
    return deferred.promise;
  }

  function getUser() {
    return user;
  }

  function signin(email, password) {
    // create a new instance of deferred
    var deferred = $q.defer();
    var lang = $rootScope.lang;
    // send a post request to the server
    $http.post('/api/v1/auth/signin/?lang=' + lang,
      { email: email, password: password })
      // handle success
      .then(function (res) {
        if(res.data && res.data.errorCode){
          user = undefined;
          deferred.reject(res);
        }
        if (res.status === 200 && res.data.user && res.data.token) {
          user = res.data.user;
          user.token = res.data.token;
          sessionStorage.setItem('Authentication', 'JWT ' + res.data.token);
          deferred.resolve(res);
        } else {
          user = undefined;
          deferred.reject(res);
        }
      })
      // handle error
      .catch(function (res) {
        user = undefined;
        deferred.reject(res);
      });

    // return promise object
    return deferred.promise;

  }

  function logout() {

    // create a new instance of deferred
    var deferred = $q.defer();

    // send a get request to the server
    $http.get('/api/v1/auth/singout')
      // handle success
      .then(function (data) {
        user = undefined;
        sessionStorage.removeItem('Authentication');
        $rootScope.cart = [];
        deferred.resolve(data);
      })
      // handle error
      .catch(function (data) {
        user = undefined;
        sessionStorage.removeItem('Authentication');
        $rootScope.cart = [];
        deferred.reject(data);
      });

    // return promise object
    return deferred.promise;

  }

  function signup(user) {

    // create a new instance of deferred
    var deferred = $q.defer();

    // send a post request to the server
    $http.post('/api/v1/auth/signup',
      { user: user })
      // handle success
      .then(function (res) {
        if (res.status === 201 && res.data.data.user) {
          // user = res.data.data.user;
          // user.token = res.data.data.token;
          deferred.resolve(res);
        } else {
          deferred.reject(res);
        }
      })
      // handle error
      .catch(function (res) {
        deferred.reject(res);
      });

    // return promise object
    return deferred.promise;

  }

  function resetPass(user) {
    // create a new instance of deferred
    var deferred = $q.defer();
    // send a post request to the server
    $http.post('/api/v1/auth/resetpassword',
      { user: user })
      // handle success
      .then(function (res) {
        if (res.status === 201 && res.data.data.email) {
          // user = res.data.data.user;
          // user.token = res.data.data.token;
          deferred.resolve(res);
        } else {
          deferred.reject(res);
        }
      })
      // handle error
      .catch(function (res) {
        deferred.reject(res);
      });

    // return promise object
    return deferred.promise;
  }

  function newPass(user) {
    // create a new instance of deferred
    var deferred = $q.defer();
    // send a post request to the server
    $http.post('/api/v1/auth/savenewpassword',
      { user: user })
      // handle success
      .then(function (res) {
        if (res.status === 200) {
          deferred.resolve(res);
        } else {
          deferred.reject(res);
        }
      })
      // handle error
      .catch(function (res) {
        deferred.reject(res);
      });

    // return promise object
    return deferred.promise;
  }

  // return available functions
  return ({
    isLoggedIn: isLoggedIn,
    getUser: getUser,
    signin: signin,
    logout: logout,
    signup: signup,
    resetPass: resetPass,
    newpass: newPass
  })
})
