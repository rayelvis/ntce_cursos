angular.module('ntce').factory('commentService', function ($q, $timeout, $http, $rootScope) {

    function create(info) {
        var deferred = $q.defer();

        var token = sessionStorage.getItem('Authentication');

        if (token) {
            // send a post request to the server
            $http.post('/api/v1/comment', info)
                // handle success
                .then(function (res) {
                    if (res.status === 201 && res.data) {
                        deferred.resolve(res);
                    } else {
                        deferred.reject(res);
                    }
                })
                // handle error
                .catch(function (res) {
                    deferred.reject(res);
                });

        } else {
            deferred.reject({ data: { message: 'No session', status: false } });
        }
        // return promise object
        return deferred.promise;
    }

    // return available functions
    return ({
        create: create
    })
})
