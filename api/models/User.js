/**
 * User
 * @description :: Model for storing users
 */
module.exports = {
  schema: true,
  attributes: {
    password: {
      type: 'string'
    },
    email: {
      type: 'string',
      email: true,
      required: true,
      unique: true
    },
    name: {
      type: 'string',
      defaultsTo: ''
    },
    photo: {
      type: 'string',
      defaultsTo: '',
      url: true
    },
    socialProfiles: {
      type: 'object',
      defaultsTo: {}
    },
    phone: {
      type: 'string',
      defaultsTo: '',
      required: true,
      unique: true
    },
    role: {
      type: 'string',
      defaultsTo: 'guess',
      required: true
    },
    activated: {
      type: 'boolean',
      defaultsTo: false
    },
    activationToken: {
      type: 'string'
    },
    resetToken: {
      type: 'string'
    },
    subscriptions:{
      collection: 'subscription',
      via: 'user'
    },
    country: {
      type: 'string'
    },
    comments: {
      collection: 'comment',
      via: 'user'
    },
    toJSON: function () {
      var obj = this.toObject();
      delete obj.password;
      delete obj.socialProfiles;
      return obj;
    }
  },
  // beforeUpdate: function (values, next) {
  //   CipherService.hashPassword(values);
  //   next();
  // },
  beforeCreate: function (values, next) {
    CipherService.hashPassword(values);
    values.activated = true; //make sure nobody is creating a user with activate set to true, this is probably just for paranoia sake
    values.activationToken = CipherService.token(new Date().getTime() + values.email);
    values.resetToken = CipherService.token(new Date().getTime() + values.email);
    next();
  }
};
