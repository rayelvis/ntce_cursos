/**
 * Course
 * @description :: Model for storing courses
 */
module.exports = {
  schema: true,
  attributes: {
    name: {
      type: 'string',
      defaultsTo: ''
    },
    englishName: {
      type: 'string',
      defaultsTo: ''
    },
    description: {
      type: 'string',
      defaultsTo: 'No description'
    },
    englishDescription: {
      type: 'string',
      defaultsTo: 'No description'
    },
    instructor: {
      model: 'Instructor'
    },
    price: {
      type: 'float',
      defaultsTo: 0.0
    },
    globalPrice: {
      type: 'float',
      defaultsTo: 0.0
    },
    currency: {
      type: 'string',
      defaultsTo: '$'
    },
    globalCurrency: {
      type: 'string',
      defaultsTo: '$'
    },
    objetivePublic: {
      type: 'longtext'
    },
    englishObjetivePublic: {
      type: 'longtext'
    },
    activated: {
      type: 'boolean',
      defaultsTo: false
    },
    duration: {
      type: 'string'
    },
    methodology: {
      type: 'string'
    },
    englishMethodology: {
      type: 'string'
    },
    resources: {
      type: 'string'
    },
    englishResources: {
      type: 'string'
    },
    contents: {
      type: 'string'
    },
    englishContents: {
      type: 'string'
    },
    modules: {
      type: 'longtext'
    },
    englishModules: {
      type: 'longtext'
    },
    whatExpect: {
      type: 'longtext'
    },
    englishWhatExpect: {
      type: 'longtext'
    },
    online: {
      type: 'boolean'
    },
    country: {
      model: 'workCountry'
    },
    starred: {
      type: 'boolean'
    },
    poster: {
      type: 'string'
    },
    date: {
      type: 'date',
      defaultsTo: '2001-01-01'
    },
    city: {
      type: 'string'
    },
    comments: {
      collection: 'comment',
      via: 'course'
    },
    subscriptions: {
      collection: 'subscription',
      via: 'course'
    },
    toJSON: function () {
      var obj = this.toObject();
      obj.rating = {};

      if (obj.comments && obj.comments.length) {
        var ratingObj = {
          one: 0,
          oneBase: 0,
          two: 0,
          twoBase: 0,
          tree: 0,
          treeBase: 0,
          four: 0,
          fourBase: 0,
          five: 0,
          fiveBase: 0
        };

        obj.comments.forEach(function (element, index) {
          switch (element.value) {
            case 1:
              ratingObj.one += 1;
              ratingObj.oneBase++;
              break;
            case 2:
              ratingObj.two += 2;
              ratingObj.twoBase++;
              break;
            case 3:
              ratingObj.tree += 3;
              ratingObj.treeBase++;
              break;
            case 4:
              ratingObj.four += 4;
              ratingObj.fourBase++;
              break;
            case 5:
              ratingObj.five += 5;
              ratingObj.fiveBase++;
              break;

            default:
              break;
          }
        }, this);

        var totalComments = ratingObj.one + ratingObj.two + ratingObj.tree + ratingObj.four + ratingObj.five;
        var rating = ((5 * ratingObj.five) + (4 * ratingObj.four) + (3 * ratingObj.tree) + (2 * ratingObj.two) + ratingObj.one) / totalComments;
        ratingObj.computed = rating;
        // rating.detail = ratingObj;
        ratingObj.totalComments = totalComments;
        ratingObj.totalCommentsBase = obj.comments.length;
        obj.rating = ratingObj;
      }

      return obj;
    }
  }
};
