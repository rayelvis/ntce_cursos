/**
 * Payment.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    code: {
      type: 'string'
    },
    user: {
      model: 'user'
    },
    amount: {
      type: 'float'
    },
    currency: {
      type: 'string'
    },
    paymentItems: {
      collection: 'paymentitem',
      via: 'payment'
    },
    checked: {
      type: 'boolean'
    }
  }
};

