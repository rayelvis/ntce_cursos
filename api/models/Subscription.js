/**
 * Suscriptions.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    course:{
      model: 'course'
    },
    user: {
      model: 'user'
    },
    dueDate: {
      type: 'date'
    },
    schoologyCode: {
      type: 'string'
    },
    active: {
      type: 'boolean'
    },
    payment: {
      model: 'payment'
    }
  }
};

