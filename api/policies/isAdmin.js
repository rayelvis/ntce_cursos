/**
 * isAuthenticated
 * @description :: Policy to inject user in req via JSON Web Token
 */
var passport = require('passport');

module.exports = function (req, res, next) {
  debugger;
  if(req.user && req.user.role){
    if(req.user.role === 'admin'){
      debugger;
      return next();
    }
  }

  return res.redirect('/#signin');
};
