/**
 * BookingController
 *
 * @description :: Server-side logic for managing Bookings
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    createBooking: function (req, res) {
        var params = req.params.all();
        Booking.create({
            name: params.name,
            email: params.email,
            phone: params.phone,
            city: params.city,
            bussiness: params.bussiness,
            message: params.message,
            course: params.course
        }).exec(function (err, record) {
            if (err) {
                res.send(500, err);
            } else {
                Course.findOne({ id: record.course }).exec(function (err2, recordCourse) {
                    if(recordCourse){
                        record.courseName = recordCourse.name;
                        record.courseId = recordCourse.id;
                    }
                        
                    //Send email
                    var emailTemplate = res.render('email/booking.ejs', { booking: record }, function (err, list) {
                        nodeMailer.send({
                            name: "Reservas NTCE",
                            from: sails.config.nodemailer.from,
                            to: sails.config.nodemailer.contact,
                            subject: 'Nueva Reserva generada desde NTCE website',
                            messageHtml: list
                        }, function (err, response) {
                            if (err) {
                                res.send(500, err);
                            } else
                                return res.ok({ message: 'Booking successfully sended.' });
                        });
                    });
                })
            }
        })
    }
};

