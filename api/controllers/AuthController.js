/**
 * AuthController
 * @description :: Server-side logic for manage user's authorization
 */
var passport = require('passport');
var bcrypt = require('bcrypt-nodejs');
/**
 * Triggers when user authenticates via passport
 * @param {Object} req Request object
 * @param {Object} res Response object
 * @param {Object} error Error object
 * @param {Object} user User profile
 * @param {Object} info Info if some error occurs
 * @private
 */
function _onPassportAuth(req, res, error, user, info) {
  if (error) return res.serverError(error);
  if (!user) {
    var mess = 'Invalid email/password combination';
    var lang = req.allParams().lang;
    if (lang && lang === 'es') {
      mess = 'Combinación usuario/contraseña incorrecta';
    }
    return res.json({
      message: mess,
      errorCode: 301
    });
  }

  return res.ok({
    // TODO: replace with new type of cipher service
    token: CipherService.createToken(user),
    user: user
  });
}

module.exports = {
  /**
   * Sign up in system
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  signup: function (req, res) {
    User
      .create(_.omit(req.allParams().user, 'id'))
      .then(function (user, err) {
        if (err) {
          res.negotiate(err);
        }
        // if (sails.config.user.requireUserActivation) {
        //   var emailTemplate = res.render('email/account-activation.ejs', { user: user }, function (err, list) {
        //     nodeMailer.send({
        //       name: user.name,
        //       from: sails.config.nodemailer.from,
        //       to: user.email,
        //       subject: 'National Training Center & Engineering Account',
        //       messageHtml: list
        //     }, function (err, response) {
        //       sails.log.debug('nodemailer sent', err, response);
        //     });
        //   });
        // }
        return res.created({
          // TODO: replace with new type of cipher service
          token: CipherService.createToken(user),
          user: user
        });
      })
      .catch(function (re) {
        if (re.code && re.messages && re.messages.email) {
          return res.json({
            message: re.messages.email[0],
            errorCode: 302
          })
        }
      })
  },

  /**
   * Sign in by local strategy in passport
   * @param {Object} req Request object
   * @param {Object} res Response object
   */
  signin: function (req, res) {

    passport.authenticate('local',
      _onPassportAuth.bind(this, req, res))(req, res);
  },
  // Logout
  singout: function (req, res) {

    console.log("logging out ......");
    req.session.destroy();
    //res.redirect('/login.html');
    res.send(200);
  },
  activate: function (req, res) {
    var params = req.params.all();

    sails.log.debug('activation action');

    //Activate the user that was requested.
    User.update({
      activationToken: params.token
    }, {
        activated: true
      }, function (err, user) {
        // Error handling
        if (err) {
          sails.log.debug(err);
          res.send(500, err);
          // Updated users successfully!
        } else {
          sails.log.debug("User activated:", user);
          res.redirect('/#/signin/?activated=true&email=' + user[0].email);
        }
      });
  },
  resetpassword: function (req, res) {
    var params = req.params.all();
    var _email = '';
    if (params && params.user) {
      _email = params.user.email;
    }

    sails.log.debug('reset password action');
    if (_email !== '') {
      User.findOne({ email: _email }, function (err, user) {
        if (err && err.name === 'UsageError') {
          return res.sendStatus(400);
        }
        else if (err && err.name === 'AdapterError' && err.code === 'E_UNIQUE') {
          return res.status(401).json(err);
        }
        else if (err) {
          console.error('Unexpected error occurred:', err.stack);
          return res.sendStatus(500);
        }
        else {
          var emailTemplate = res.render('email/reset-password.ejs', { user: user }, function (err, list) {
            nodeMailer.send({
              name: user.name,
              from: sails.config.nodemailer.from,
              to: user.email,
              subject: 'Reset your NTCE password',
              messageHtml: list
            }, function (err, response) {
              if (err) {
                res.send(500, err);
              } else
                return res.ok({
                  email: _email || ''
                });
            });
          });
        }
      });
    }


  },

  newPassword: function (req, res) {
    var params = req.params.all();
    sails.log.debug('bypass new password action');

    var newRT = CipherService.token(new Date().getTime() + "NTCE");
    //Activate the user that was requested.
    User.update({
      id: params.id,
      resetToken: params.token
    },
      {
        resetToken: newRT
      },
      function (err, user) {
        // Error handling
        if (err) {
          sails.log.debug(err);
          res.redirect('/#/signin');
        } else {
          sails.log.debug("Setting new password:", user);
          res.redirect('/#/newpassword/?token=' + newRT);
        }
      });
  },

  saveNewPassword: function (req, res) {
    var params = req.params.all();
    sails.log.debug('save new password action');
    User.update({
      resetToken: params.user.token
    }, {
        password: bcrypt.hashSync(params.user.password)
      }, function (err, user) {
        // Error handling
        if (err) {
          sails.log.debug(err);

          res.send(500, err);
          // Updated users successfully!
        } else {
          sails.log.debug("User activated:", user);
          res.send(200);
        }
      });
  }
};
