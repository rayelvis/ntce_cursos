/**
 * CourseController
 *
 * @description :: Server-side logic for managing Courses
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var uuid = require('node-uuid');

module.exports = {
  findByFilters: function (req, res) {
    var filters = {};
    var preparedFilters = {};

    //Set params to filters var
    if (req.query.online || req.query.name || req.query.country) {
      filters = req.query;
    }

    //Verify if name
    if (filters.name && filters.name !== '') {
      preparedFilters.name = {
        contains: filters.name
      };
    }

    //Verify if online
    if (filters.online) {
      if (filters.online == "true")
        preparedFilters.online = true;
      else if (filters.online == "false")
        preparedFilters.online = false;
    }

    //Filter by country is runned after find in db becouse is more esay with the relationship and are a short courses array
    Course
      .find(preparedFilters)
      .populate('country')
      .exec(function (err, foundCourses) {
        if (err) {
          res.send(500, err);
        }

        //Apply country filter
        if (filters.country) {
          foundCourses = foundCourses.filter(function(c){
            if(c.country && c.country.id && c.country.id === filters.country){
              return c;
            }
          });
        }

        res.ok({
          courses: foundCourses
        });
      })
  },
  uploadCourse: function (req, res) {

    var params = req.allParams();
    req.uniqueFileName = uuid.v1();

    req.file('file').upload({
      dirname: require('path').resolve(sails.config.appPath, 'assets/uploads/courses'),
      saveAs: req.uniqueFileName + ".png"
    }, function (err, uploadedFiles) {
      sails.log.debug('uploaded in: ' + req.uniqueFileName + ".png");
    });

    //Now register in db
    Course.create({
      name: params['name'],
      englishName: params['englishName'],
      description: params['description'],
      englishDescription: params['englishDescription'],
      instructor: params['instructor'],
      price: params['price'],
      globalPrice: params['globalPrice'],
      currency: 'BsF',
      globalCurrency: '$',
      objetivePublic: params['objetivePublic'],
      englishObjetivePublic: params['englishObjetivePublic'],
      activated: params['activated'],
      duration: params['duration'],
      methodology: params['methodology'],
      englishMethodology: params['englishMethodology'],
      resources: params['resources'],
      englishResources: params['englishResources'],
      contents: params['contents'],
      englishContents: params['englishContents'],
      modules: params['modules'],
      englishModules: params['englishModules'],
      whatExpect: params['whatExpect'],
      englishWhatExpect: params['englishWhatExpect'],
      online: params['online'],
      starred: params['starred'],
      poster: "/uploads/courses/" + req.uniqueFileName + ".png",
      city: params['city'],
      country: params['country'],
      date: params['date']
    }).exec(function (err, record) {
      if (err) return res.negotiate(err);
      return res.json({
        message: ' course(s) uploaded successfully!'
      });
    })
  },
  getAllCourses: function (req, res) {
    Course
      .find({ activated: true })
      .populate('instructor')
      .exec(function (err, courses) {
        if (err) return res.negotiate(err);

        res.ok({
          courses: courses
        });
      })
  },
  getStarredCourses: function (req, res) {
    Course
      .find({ starred: true, activated: true })
      .populate('instructor')
      .exec(function (err, courses) {
        if (err) return res.negotiate(err);
        res.ok({
          courses: courses
        });
      })
  },
  updateCourse: function (req, res) {
    var params = req.allParams();
    if (params['id']) {
      req.uniqueFileName = uuid.v1();

      req.file('file').upload({
        dirname: require('path').resolve(sails.config.appPath, 'assets/uploads/courses'),
        saveAs: req.uniqueFileName + ".png"
      }, function (err, uploadedFiles) {
        if (err)
          debugger;
        if (uploadedFiles.length > 0) {
          sails.log.debug('uploaded in: ' + req.uniqueFileName + ".png");
          Course.update({ id: params['id'] },
            { poster: "/uploads/courses/" + req.uniqueFileName + ".png" })
            .exec(function afterwards(err, record) { })
        }



      });

      //Now register in db
      Course.update({ id: params['id'] }, {
        name: params['name'],
        englishName: params['englishName'],
        description: params['description'],
        englishDescription: params['englishDescription'],
        instructor: params['instructor'],
        price: params['price'],
        globalPrice: params['globalPrice'],
        currency: 'BsF',
        globalCurrency: '$',
        objetivePublic: params['objetivePublic'],
        englishObjetivePublic: params['englishObjetivePublic'],
        activated: params['activated'],
        duration: params['duration'],
        methodology: params['methodology'],
        englishMethodology: params['englishMethodology'],
        resources: params['resources'],
        englishResources: params['englishResources'],
        contents: params['contents'],
        englishContents: params['englishContents'],
        modules: params['modules'],
        englishModules: params['englishModules'],
        whatExpect: params['whatExpect'],
        englishWhatExpect: params['englishWhatExpect'],
        online: params['online'],
        starred: params['starred'],
        city: params['city'],
        country: params['country'],
        date: params['date']
      }).exec(function afterwards(err, record) {
        if (err) return res.negotiate(err);

        return res.json({
          message: 'course updated successfully!'
        });
      })
    }
  }
};

