/**
 * AdminController
 *
 *
 * Controller for admin
 * @author: bersnard Coello
 */

module.exports = {
  enter: function (req, res) {
    res.render('admin/dashboard');
  }
};
