/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
  isAuthenticated: function (req, res) {
    if (req.isAuthenticated()) {
      return res.ok({
        // TODO: replace with new type of cipher service
        user: req.user
      });
    } else {
      return res.send(401)
    }

  },
  contactMe: function (req, res) {

    var params = req.params.all();

    //Send email
    var emailTemplate = res.render('email/contact.ejs', { form: params }, function (err, list) {
      nodeMailer.send({
        name: params.name,
        from: sails.config.nodemailer.from,
        to: sails.config.nodemailer.contact,
        subject: 'Contact Request from Website',
        messageHtml: list
      }, function (err, response) {
        if (err) {
          res.send(500, err);
        } else
          return res.ok({ message: 'Contact email sended' });
      });
    });
  }
};

