/**
 * SuscriptionsController
 *
 * @description :: Server-side logic for managing Suscriptions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

var paypal = require('paypal-rest-sdk');
var _ = require('lodash');
var uuid = require('node-uuid');

//Members
var items = [];

paypal.configure({
    'mode': 'sandbox', //sandbox or live
    'client_id': 'AdeflBMVSclzNE1NQ7HkBTMiRDYxIVrhE9nY9TQXn5FsW-gEjO8AT-QcxhaISJq0ON_gpLPcESi6JsWM',
    'client_secret': 'EI-QRBZl-5ZPVFwT7NQHRA4Rgxj0cswPU1DCbOccYZh1TtnPnS8EUSXaETJapj8vYV_UOlyCIDzw2-2p'
})

function _savePayment(PaymentObj, User) {

    //PaymentItem constructor
    var _paymentItemObj = function (course, price, qty) {
        this.course = course; //Course unique ID
        this.price = price;
        this.qty = qty;
    };
    var saleTotal = 0.00;
    var saleCurrency = PaymentObj.transactions[0].amount.currency;

    PaymentObj.transactions.forEach(function (t, i) {
        saleTotal += parseFloat(t.amount.total);
    });

    var _this = this;

    debugger;
    Payment.create({
        code: uuid.v1(),
        user: User,
        amount: saleTotal,
        currency: saleCurrency,
        checked: false
    }).exec(function (err, record) {
        if (err)
            console.log('error al salvar record');
        else {
            // Create paymentItems from paypal response
            PaymentObj.transactions.forEach(function (r) {
                r.item_list.items.forEach(function (t, index) {
                    PaymentItem.create({
                        course: t.sku,
                        amount: parseFloat(t.price),
                        currency: t.currency,
                        qty: t.quantity,
                        payment: record.id
                    }).exec(function (err2, record2) {
                        if (err2)
                            console.log(err2);
                        else {
                            //Create course subscription
                            Subscription.create({
                                course: t.sku,
                                user: User,
                                active: false,
                                payment: record.id
                            }).exec(function (err3, record3) {
                                if (err3)
                                    console.log(err3);
                            })
                        }

                    });
                });
            });
        }
    })
}

module.exports = {
    payWithPaypal: function (req, res) {
        var params = req.allParams();
        //Prepare Items array from query params
        for (var prop in params) {
            if (prop.indexOf('item') > -1) {
                var item = params[prop];
                var itemArr = item.split('_');
                items.push({
                    sku: itemArr[0],
                    quantity: parseInt(itemArr[1])
                })
            }
        }

        //Create array of courses ids to search in db
        var onlyIds = [];
        items.forEach(function (el, i) {
            onlyIds.push(el.sku);
        })

        //Set vars as global to access them inside query callback
        global.items = items;
        global.paypal = paypal;

        Course.find({
            id: onlyIds
        }).exec(function (err, courses) {
            if (err) {
                res.negotiate(err);
            }

            var totalToCharge = 0.00;
            courses.forEach(function (c, index) {

                //Set price to items
                var i = items[index];
                i.price = c.globalPrice;
                i.currency = "USD";
                i.name = "Compra de curso '" + c.name + "'";

                //increse total
                totalToCharge = totalToCharge + (i.quantity * i.price);
            })

            //Create payment object
            var create_payment_json = {
                "intent": "sale",
                "payer": {
                    "payment_method": "paypal"
                },
                "redirect_urls": {
                    "return_url": sails.config.HOST + "/paymentgateway/success?user=" + params.user,
                    "cancel_url": sails.config.HOST + "/paymentgateway/cancel"
                },
                "transactions": [{
                    "item_list": {
                        "items": items
                    },
                    "amount": {
                        "currency": "USD",
                        "total": totalToCharge
                    },
                    "description": "NTCE payment proccess V1."
                }]
            };

            //First paypal call
            paypal.payment.create(create_payment_json, function (error, payment) {
                if (error) {
                    throw error;
                } else {
                    for (var i = 0; i < payment.links.length; i++) {
                        if (payment.links[i].rel === 'approval_url') {
                            res.redirect(payment.links[i].href);
                        }
                    }
                }
            });
        })
    },
    paySuccess: function (req, res) {
        var payerId = req.query.PayerID;
        var paymentId = req.query.paymentId;
        var userId = req.query.user;
        var ammount = 0;

        var execute_payment_json = {
            "payer_id": payerId
        };

        global.user = userId;

        function paypalCallback(error, payment) {
            if (error) {
                console.log(error.response);
                throw error;
            } else {
                // console.log('Get payment response');
                // console.log(JSON.stringify(payment));
                _savePayment(payment, user);
                res.redirect('/#/sale/success');
            }
        }

        paypal.payment.execute(paymentId, execute_payment_json, paypalCallback);
    },

    payCancel: function (req, res) {
        res.send('Error processing the payment');
    },

    lockSubscription: function (req, res) {
        var params = req.params.all();

        Subscription.update({
            id: params.id
        }, {
                active: params.lock
            }).exec(function (err, record) {
                if (err)
                    return res.negotiate(err);
                else
                    return res.ok({ message: 'Subscription modified' })
            })
    },

    getFromUser: function (req, res) {
        User
            .find({ id: req.allParams().userId })
            .populate(["subscriptions"])
            .exec(function (err, subs) {
                if (err)
                    return res.negotiate(err);
                else
                    return res.ok({
                        obj: subs
                    });
            });
    }
};

