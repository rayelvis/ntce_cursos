/**
 * PaymentController
 *
 * @description :: Server-side logic for managing Payments
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    check: function (req, res) {
        var params = req.params.all();

        Payment.update({ id: params.id },
            {
                checked: params.check
            }).exec(function(err, record){
                if(err)
                    return res.negotiate(err);
                else
                    return res.ok({message: 'Sale updated'});
            })
    }
};

