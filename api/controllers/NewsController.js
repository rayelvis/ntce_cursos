/**
 * NewsController
 *
 * @description :: Server-side logic for managing news
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    addToNews: function (req, res) {
        var params = req.params.all();

        News.create({
            name: params.name,
            emailAddress: params.emailAddress
        }).exec(function (err, record) {

            if (err) {
                res.send(500, err);
            } else {
                //Send email
                var emailTemplate = res.render('email/newsletter.ejs', { user: record }, function (err, list) {
                    nodeMailer.send({
                        name: record.name,
                        from: sails.config.nodemailer.from,
                        to: record.emailAddress,
                        subject: 'Welcome to NTCE newsletters',
                        messageHtml: list
                    }, function (err, response) {
                        if (err) {
                            res.send(500, err);
                        } else
                            return res.ok({ message: 'Subscription success' });
                    });
                });
            }
        })
    }
};

