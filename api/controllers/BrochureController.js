/**
 * BrochureController
 *
 * @description :: Server-side logic for managing Brochures
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	update: function(req, res){
        var params = req.allParams();
    
        req.file('file').upload({
          dirname: require('path').resolve(sails.config.appPath, 'assets/uploads/brochure'),
          saveAs: "brochure-current.pdf"
        }, function (err, uploadedFiles) {
            if(err)
                return res.negotiate(err);
            else{
                sails.log.debug('uploaded in: ' + req.uniqueFileName + ".png");
                return res.ok({message: 'Brochure updated!'})
            }
          
        });
    }
};

